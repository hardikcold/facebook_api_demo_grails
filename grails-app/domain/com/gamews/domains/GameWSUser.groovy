package com.gamews.domains

class GameWSUser {

	String userUUID
	String username
	String fullName
	String userProfile
	String loginType
	String userToken
	String others
	Date dateCreated
	Date lastUpdated
	Boolean status = true
	
	static hasMany = [friendsList : FriendsList]
	static mapping = {
		userProfile sqlType : "text"
	}
    static constraints = {
		userUUID blank : true, nullable : true, unique : true
		username blank : true, nullable : true, unique : true
		loginType blank : true, nullable : true, inList : ["FB", "GP", "LI"]
		userToken blank : true, nullable : true
		others blank : true, nullable : true
		friendsList blank : true, nullable : true
    }
}
