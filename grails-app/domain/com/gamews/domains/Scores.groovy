package com.gamews.domains

class Scores {

	String totalCoins
	String score
	String userUUID
	Date dateCreated
	Date lastUpdate
	Boolean status = true
	
    static constraints = {
		
    }
}
