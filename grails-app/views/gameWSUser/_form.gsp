<%@ page import="com.gamews.domains.GameWSUser" %>



<div class="fieldcontain ${hasErrors(bean: gameWSUserInstance, field: 'userUUID', 'error')} ">
	<label for="userUUID">
		<g:message code="gameWSUser.userUUID.label" default="User UUID" />
		
	</label>
	<g:textField name="userUUID" value="${gameWSUserInstance?.userUUID}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: gameWSUserInstance, field: 'username', 'error')} ">
	<label for="username">
		<g:message code="gameWSUser.username.label" default="Username" />
		
	</label>
	<g:textField name="username" value="${gameWSUserInstance?.username}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: gameWSUserInstance, field: 'loginType', 'error')} ">
	<label for="loginType">
		<g:message code="gameWSUser.loginType.label" default="Login Type" />
		
	</label>
	<g:select name="loginType" from="${gameWSUserInstance.constraints.loginType.inList}" value="${gameWSUserInstance?.loginType}" valueMessagePrefix="gameWSUser.loginType" noSelection="['': '']"/>

</div>

<div class="fieldcontain ${hasErrors(bean: gameWSUserInstance, field: 'userToken', 'error')} ">
	<label for="userToken">
		<g:message code="gameWSUser.userToken.label" default="User Token" />
		
	</label>
	<g:textField name="userToken" value="${gameWSUserInstance?.userToken}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: gameWSUserInstance, field: 'status', 'error')} ">
	<label for="status">
		<g:message code="gameWSUser.status.label" default="Status" />
		
	</label>
	<g:checkBox name="status" value="${gameWSUserInstance?.status}" />

</div>

