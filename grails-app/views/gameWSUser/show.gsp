
<%@ page import="com.gamews.domains.GameWSUser" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'gameWSUser.label', default: 'GameWSUser')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-gameWSUser" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-gameWSUser" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list gameWSUser">
			
				<g:if test="${gameWSUserInstance?.userUUID}">
				<li class="fieldcontain">
					<span id="userUUID-label" class="property-label"><g:message code="gameWSUser.userUUID.label" default="User UUID" /></span>
					
						<span class="property-value" aria-labelledby="userUUID-label"><g:fieldValue bean="${gameWSUserInstance}" field="userUUID"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${gameWSUserInstance?.username}">
				<li class="fieldcontain">
					<span id="username-label" class="property-label"><g:message code="gameWSUser.username.label" default="Username" /></span>
					
						<span class="property-value" aria-labelledby="username-label"><g:fieldValue bean="${gameWSUserInstance}" field="username"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${gameWSUserInstance?.loginType}">
				<li class="fieldcontain">
					<span id="loginType-label" class="property-label"><g:message code="gameWSUser.loginType.label" default="Login Type" /></span>
					
						<span class="property-value" aria-labelledby="loginType-label"><g:fieldValue bean="${gameWSUserInstance}" field="loginType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${gameWSUserInstance?.userToken}">
				<li class="fieldcontain">
					<span id="userToken-label" class="property-label"><g:message code="gameWSUser.userToken.label" default="User Token" /></span>
					
						<span class="property-value" aria-labelledby="userToken-label"><g:fieldValue bean="${gameWSUserInstance}" field="userToken"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${gameWSUserInstance?.dateCreated}">
				<li class="fieldcontain">
					<span id="dateCreated-label" class="property-label"><g:message code="gameWSUser.dateCreated.label" default="Date Created" /></span>
					
						<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${gameWSUserInstance?.dateCreated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${gameWSUserInstance?.lastUpdated}">
				<li class="fieldcontain">
					<span id="lastUpdated-label" class="property-label"><g:message code="gameWSUser.lastUpdated.label" default="Last Updated" /></span>
					
						<span class="property-value" aria-labelledby="lastUpdated-label"><g:formatDate date="${gameWSUserInstance?.lastUpdated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${gameWSUserInstance?.status}">
				<li class="fieldcontain">
					<span id="status-label" class="property-label"><g:message code="gameWSUser.status.label" default="Status" /></span>
					
						<span class="property-value" aria-labelledby="status-label"><g:formatBoolean boolean="${gameWSUserInstance?.status}" /></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:gameWSUserInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${gameWSUserInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
