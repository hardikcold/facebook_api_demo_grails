
<%@ page import="com.gamews.domains.Scores" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'scores.label', default: 'Scores')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-scores" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-scores" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list scores">
			
				<g:if test="${scoresInstance?.score}">
				<li class="fieldcontain">
					<span id="score-label" class="property-label"><g:message code="scores.score.label" default="Score" /></span>
					
						<span class="property-value" aria-labelledby="score-label"><g:fieldValue bean="${scoresInstance}" field="score"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${scoresInstance?.totalCoins}">
				<li class="fieldcontain">
					<span id="totalCoins-label" class="property-label"><g:message code="scores.totalCoins.label" default="Total Coins" /></span>
					
						<span class="property-value" aria-labelledby="totalCoins-label"><g:fieldValue bean="${scoresInstance}" field="totalCoins"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${scoresInstance?.userUUID}">
				<li class="fieldcontain">
					<span id="userUUID-label" class="property-label"><g:message code="scores.userUUID.label" default="User UUID" /></span>
					
						<span class="property-value" aria-labelledby="userUUID-label"><g:fieldValue bean="${scoresInstance}" field="userUUID"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:scoresInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${scoresInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
