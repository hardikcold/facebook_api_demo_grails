<%@ page import="com.gamews.domains.Scores" %>



<div class="fieldcontain ${hasErrors(bean: scoresInstance, field: 'score', 'error')} ">
	<label for="score">
		<g:message code="scores.score.label" default="Score" />
		
	</label>
	<g:textField name="score" value="${scoresInstance?.score}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: scoresInstance, field: 'totalCoins', 'error')} ">
	<label for="totalCoins">
		<g:message code="scores.totalCoins.label" default="Total Coins" />
		
	</label>
	<g:textField name="totalCoins" value="${scoresInstance?.totalCoins}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: scoresInstance, field: 'userUUID', 'error')} ">
	<label for="userUUID">
		<g:message code="scores.userUUID.label" default="User UUID" />
		
	</label>
	<g:textField name="userUUID" value="${scoresInstance?.userUUID}"/>

</div>

