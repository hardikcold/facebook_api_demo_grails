
<%@ page import="com.gamews.domains.Scores" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'scores.label', default: 'Scores')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-scores" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-scores" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="score" title="${message(code: 'scores.score.label', default: 'Score')}" />
					
						<g:sortableColumn property="totalCoins" title="${message(code: 'scores.totalCoins.label', default: 'Total Coins')}" />
					
						<g:sortableColumn property="userUUID" title="${message(code: 'scores.userUUID.label', default: 'User UUID')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${scoresInstanceList}" status="i" var="scoresInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${scoresInstance.id}">${fieldValue(bean: scoresInstance, field: "score")}</g:link></td>
					
						<td>${fieldValue(bean: scoresInstance, field: "totalCoins")}</td>
					
						<td>${fieldValue(bean: scoresInstance, field: "userUUID")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${scoresInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
