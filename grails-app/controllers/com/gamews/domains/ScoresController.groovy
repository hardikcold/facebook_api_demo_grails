package com.gamews.domains



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class ScoresController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Scores.list(params), model:[scoresInstanceCount: Scores.count()]
    }

    def show(Scores scoresInstance) {
        respond scoresInstance
    }

    def create() {
        respond new Scores(params)
    }

    @Transactional
    def save(Scores scoresInstance) {
        if (scoresInstance == null) {
            notFound()
            return
        }

        if (scoresInstance.hasErrors()) {
            respond scoresInstance.errors, view:'create'
            return
        }

        scoresInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'scoresInstance.label', default: 'Scores'), scoresInstance.id])
                redirect scoresInstance
            }
            '*' { respond scoresInstance, [status: CREATED] }
        }
    }

    def edit(Scores scoresInstance) {
        respond scoresInstance
    }

    @Transactional
    def update(Scores scoresInstance) {
        if (scoresInstance == null) {
            notFound()
            return
        }

        if (scoresInstance.hasErrors()) {
            respond scoresInstance.errors, view:'edit'
            return
        }

        scoresInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Scores.label', default: 'Scores'), scoresInstance.id])
                redirect scoresInstance
            }
            '*'{ respond scoresInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Scores scoresInstance) {

        if (scoresInstance == null) {
            notFound()
            return
        }

        scoresInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Scores.label', default: 'Scores'), scoresInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'scoresInstance.label', default: 'Scores'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
