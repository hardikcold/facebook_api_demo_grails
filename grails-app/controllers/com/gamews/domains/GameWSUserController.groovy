package com.gamews.domains



import static org.springframework.http.HttpStatus.*
import grails.async.Promise;
import grails.transaction.Transactional
import static grails.async.Promises.*
@Transactional(readOnly = true)
class GameWSUserController {

	def facebookGraphService
	static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

	def index(Integer max) {
		def facebookFriends
		def friendListInstance
		def isFriendJoined
		def userFbUUID = session.facebook.uid
		params.max = Math.min(max ?: 10, 100)
		println "Facebook UUId :" + userFbUUID
		def userInstance = GameWSUser.findByUserUUID(userFbUUID) ?: new GameWSUser()
		println "userInstance :" + userInstance
		userInstance.userUUID = userFbUUID
		def facebookProfile1 = facebookGraphService.getFacebookProfile(id : session.facebook.uid)
		if(userFbUUID){
			def facebookProfile = getFBProfile(userFbUUID)
			println "FB Profile :" + facebookProfile
			userInstance.username = facebookProfile?.username
			userInstance.fullName = facebookProfile?.name
			userInstance.userProfile = facebookProfile?.toString()
			userInstance.loginType = "FB"
			userInstance.others = "Test"


			//def p = GameWSUser.async.task {
			//facebookFriends  = facebookGraphService.getFriends(offset : "0", limit : "10")
			facebookFriends  = facebookGraphService.getFriends()
			println "facebookFriends :" + facebookFriends
			if(facebookFriends){
				facebookFriends.data.each{
					friendListInstance = new FriendsList()
					friendListInstance.fbUUID = it.id
					isFriendJoined = GameWSUser.findByUserUUID(it.id)
					println "isFriendJoined :" + isFriendJoined
					if(isFriendJoined != null){
						def isAlreadyInFriendList
						if(userInstance?.id)
							isAlreadyInFriendList = FriendsList.findByFbUUIDAndUser(it.id, userInstance)

						if(!isAlreadyInFriendList){
							friendListInstance.fullName = it.name
							def profilePic = getFBProfilePic(it.id)
							println "profilePic :" + profilePic
							friendListInstance.profilePic = profilePic

							facebookProfile = getFBProfile(it.id)
							println "Frnd Username :" + facebookProfile.username
							friendListInstance.username = facebookProfile.username

							friendListInstance.friendProfile = facebookProfile.toString()
							userInstance.addToFriendsList(friendListInstance)
						}
					}
				}
			}
			userInstance.save(flush:true)
			//}
			//respond GameWSUser.list(params), model:[gameWSUserInstanceCount: GameWSUser.count(), facebookFriends : facebookFriends?.data]
			//return
		}
		println "Frind List :" + userInstance.friendsList
		respond userInstance, model : [facebookFriends : userInstance.friendsList]
	}

	def getFBProfilePic(def id){
		def profilePic = facebookGraphService.getProfilePhotoSrc(id)
		return profilePic
	}

	def getFBProfile(def fbUUID){

		def profile = facebookGraphService.getFacebookProfile(id : fbUUID.toString())
		println "Profile :" + profile
		return profile
	}

	def show(GameWSUser gameWSUserInstance) {
		respond gameWSUserInstance
	}

	def create() {

		respond new GameWSUser(params)
	}

	@Transactional
	def save(GameWSUser gameWSUserInstance) {
		if (gameWSUserInstance == null) {
			notFound()
			return
		}

		if (gameWSUserInstance.hasErrors()) {
			respond gameWSUserInstance.errors, view:'create'
			return
		}

		gameWSUserInstance.save flush:true

		request.withFormat {
			form multipartForm {
				flash.message = message(code: 'default.created.message', args: [
					message(code: 'gameWSUserInstance.label', default: 'GameWSUser'),
					gameWSUserInstance.id
				])
				redirect gameWSUserInstance
			}
			'*' { respond gameWSUserInstance, [status: CREATED] }
		}
	}

	def edit(GameWSUser gameWSUserInstance) {
		respond gameWSUserInstance
	}

	@Transactional
	def update(GameWSUser gameWSUserInstance) {
		if (gameWSUserInstance == null) {
			notFound()
			return
		}

		if (gameWSUserInstance.hasErrors()) {
			respond gameWSUserInstance.errors, view:'edit'
			return
		}

		gameWSUserInstance.save flush:true

		request.withFormat {
			form multipartForm {
				flash.message = message(code: 'default.updated.message', args: [
					message(code: 'GameWSUser.label', default: 'GameWSUser'),
					gameWSUserInstance.id
				])
				redirect gameWSUserInstance
			}
			'*'{ respond gameWSUserInstance, [status: OK] }
		}
	}

	@Transactional
	def delete(GameWSUser gameWSUserInstance) {

		if (gameWSUserInstance == null) {
			notFound()
			return
		}

		gameWSUserInstance.delete flush:true

		request.withFormat {
			form multipartForm {
				flash.message = message(code: 'default.deleted.message', args: [
					message(code: 'GameWSUser.label', default: 'GameWSUser'),
					gameWSUserInstance.id
				])
				redirect action:"index", method:"GET"
			}
			'*'{ render status: NO_CONTENT }
		}
	}

	protected void notFound() {
		request.withFormat {
			form multipartForm {
				flash.message = message(code: 'default.not.found.message', args: [
					message(code: 'gameWSUserInstance.label', default: 'GameWSUser'),
					params.id
				])
				redirect action: "index", method: "GET"
			}
			'*'{ render status: NOT_FOUND }
		}
	}
}
